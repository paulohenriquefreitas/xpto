package br.com.xpto.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;


import br.com.xpto.model.PersonEntity;
import br.com.xpto.repository.PersonRepositoryImpl;
import io.spring.guides.gs_producing_web_service.AddPersonRequest;
import io.spring.guides.gs_producing_web_service.AddPersonResponse;
import io.spring.guides.gs_producing_web_service.DeletePersonRequest;
import io.spring.guides.gs_producing_web_service.GetPersonRequest;
import io.spring.guides.gs_producing_web_service.GetPersonResponse;
import io.spring.guides.gs_producing_web_service.UpdatePersonRequest;
import io.spring.guides.gs_producing_web_service.UpdatePersonResponse;

@Endpoint
public class PersonEndpoint {

    private static final String NAMESPACE_URI = "http://localhost:8080/xml/person";

    private PersonRepositoryImpl personRepository;

    @Autowired
    public PersonEndpoint(PersonRepositoryImpl personRepository) {
        this.personRepository = personRepository;
    }
    
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPersonRequest")
	@ResponsePayload
    public GetPersonResponse getPerson (@RequestPayload GetPersonRequest request) {
    	GetPersonResponse gpr = new GetPersonResponse();
    	gpr.setPerson(personRepository.findByName(request.getName()));
		return gpr;

    }
    
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "AddPersonRequest")
	@ResponsePayload
    public AddPersonResponse addPerson (@RequestPayload AddPersonRequest request) {
    	PersonEntity person = new PersonEntity();
    	person.setAge(request.getAge());
    	person.setName(request.getName());
    	AddPersonResponse apr = new AddPersonResponse();
    	apr.setPerson(personRepository.save(person));
		return apr;

    }
    
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "UpdatePersonRequest")
	@ResponsePayload
    public UpdatePersonResponse updatePerson (@RequestPayload UpdatePersonRequest request) {
    	PersonEntity person = new PersonEntity();
    	person.setAge(request.getAge());
    	person.setName(request.getName());
    	UpdatePersonResponse upr = new UpdatePersonResponse();
    	upr.setPerson(personRepository.updatePerson(person));
		return upr;

    }
    
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "DeletePersonRequest")
	@ResponsePayload
    public void deletePerson (@RequestPayload DeletePersonRequest request) {
    	personRepository.delete(request.getName());
		

    }
}
