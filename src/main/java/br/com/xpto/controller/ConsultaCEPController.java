package br.com.xpto.controller;

import java.rmi.RemoteException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.correios.bsb.sigep.master.bean.cliente.AtendeClienteProxy;
import br.com.correios.bsb.sigep.master.bean.cliente.EnderecoERP;
import br.com.correios.bsb.sigep.master.bean.cliente.SQLException;
import br.com.correios.bsb.sigep.master.bean.cliente.SigepClienteException;

@Path("/consulta")
public class ConsultaCEPController {	
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ConsultaCEPController.class);

	@Path("{cep}")
	@GET
	@Produces("application/json")
	public EnderecoERP consultarCEP(@PathParam("cep") String cep) {
		try {
			return new AtendeClienteProxy().consultaCEP(cep);
		} catch (SQLException e) {			
			LOGGER.error("Ocorreu um erro ao consultar o cep.", e.getMessage());			
		} catch (SigepClienteException e) {			
			LOGGER.error("Ocorreu um erro ao consultar o cep.", e.getMessage());
		} catch (RemoteException e) {
			LOGGER.error("Ocorreu um erro ao consultar o cep.", e.getMessage());
		}
		return new EnderecoERP();
	}
}
