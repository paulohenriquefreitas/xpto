package br.com.xpto.repository;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import br.com.xpto.model.PersonEntity;
import io.spring.guides.gs_producing_web_service.Person;




@Component
public class PersonRepositoryImpl implements PersonRepository {
	
	@Autowired
    private JdbcTemplate jdbcTemplate;

	@Override
	public Person save(PersonEntity person) {
		jdbcTemplate.update("INSERT INTO person(name, age) VALUES (?,?)", person.getName(),person.getAge());
		Person p = new Person();
		BeanUtils.copyProperties(p, person);
		return p;
	}

	@Override
	public Person findByName(String name) {
		String query = "SELECT * FROM PERSON WHERE ID = ?";
		PersonEntity person = jdbcTemplate.queryForObject(
		    query, new Object[] { name }, new PersonRowMapper());
		Person p = new Person();
		BeanUtils.copyProperties(p, person);
		return p;
	}

	@Override
	public Person updatePerson(PersonEntity person) {
		String SQL = "UPDATE PERSON SET AGE = ?, NAME = ? WHERE ID = ?";
		jdbcTemplate.update(SQL, person.getAge(), person.getAge(), person.getId());
	    return this.findByName(person.getName());
	}

	@Override
	public void delete(String name) {
		String deleteSql = "DELETE FROM employee WHERE id = ?";
		jdbcTemplate.update(deleteSql, name);
		
	}


}
