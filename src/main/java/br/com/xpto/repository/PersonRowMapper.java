package br.com.xpto.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import br.com.xpto.model.PersonEntity;

public class PersonRowMapper implements RowMapper<PersonEntity> {

	@Override
	public PersonEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
		PersonEntity person = new PersonEntity();
		person.setId(rs.getInt("id"));
		person.setAge(rs.getInt("age"));
		person.setName(rs.getString("name"));
		return person;
	}
}