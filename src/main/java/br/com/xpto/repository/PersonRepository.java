package br.com.xpto.repository;

import br.com.xpto.model.PersonEntity;
import io.spring.guides.gs_producing_web_service.Person;

public interface PersonRepository {
	
	Person save(PersonEntity person);
	Person findByName(String name);
	Person updatePerson(PersonEntity person);
	void delete (String name);

}
